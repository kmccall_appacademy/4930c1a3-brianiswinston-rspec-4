class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    digs = @seconds
    hour = 0
    min = 0
    sec = 0

    until digs == 0
      if digs >= 3600
        hour += 1
        digs -= 3600
      elsif digs >= 60
        min += 1
        digs -= 60
      else
        sec += digs
        digs -= digs
      end
    end

    "#{padded(hour)}:#{padded(min)}:#{padded(sec)}"
  end

  def padded(secs)
    secs < 10 ? "0#{secs}" : "#{secs}"
  end
end
