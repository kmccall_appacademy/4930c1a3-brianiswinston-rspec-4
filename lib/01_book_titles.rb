class Book
  attr_writer :title

  def title
    nos = %w[and the a an in of]
    @title.split.map.with_index { |word, idx| (!nos.include?(word) || idx == 0) ? word.capitalize : word }.join(' ')
  end
end
