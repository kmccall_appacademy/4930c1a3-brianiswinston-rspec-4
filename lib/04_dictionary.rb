class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(hash)
    hash = {hash => nil} unless hash.class == Hash
    hash.each { |word, defi| @entries[word] = defi }
  end

  def keywords
    @entries.keys.sort
  end

  def include?(str)
    @entries.keys.include?(str)
  end

  def find(str)
    answer = {}
    @entries.each { |word, defi| answer[word] = defi if word.include?(str) }
    answer
  end

  def printable
    answer = []
    keywords.each do |key|
      answer << "[#{key}] \"#{@entries[key]}\""
    end
    answer.join("\n")
  end
end
