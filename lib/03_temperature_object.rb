class Temperature
  def initialize(options = {})
    @fah = options[:f] || (options[:c] / (5/9.to_f)) + 32
    @cel = options[:c] || (options[:f] - 32) * (5/9.to_f)
  end

  def in_fahrenheit
    @fah
  end

  def in_celsius
    @cel
  end

  def Temperature.from_celsius(num)
    Temperature.new(c: num)
  end

  def Temperature.from_fahrenheit(num)
    Temperature.new(f: num)
  end
end

class Celsius < Temperature
  def initialize(celsius)
    super(c: celsius)
  end
end

class Fahrenheit < Temperature
  def initialize(fahrenheit)
    super(f: fahrenheit)
  end
end
